function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    if (!elements || !cb)
    {
        return []
    }
    let arr = []
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index]))
        {
            arr.push(elements[index])
        }
    }
    return arr;
}

const amIOdd = (num) => {
    if (num % 2 == 1)
    {
        return true
    }
    else
    {
        return false
    }
}

module.exports = {
    filter,
    amIOdd
}