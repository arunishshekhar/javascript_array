const file = require('../flatten')

const nestedArray = [1,[2,['hi']],[[[[8]]]]] ;

console.log(file.flatten(nestedArray))
console.log(file.flatten([1, [2], [[3],14,15], [[[4,5,6],7,8,9],10,11],12,13]))
console.log(file.flatten([1, 2, 3, 4, 5, 6, 7, 8]))
console.log(file.flatten([0, 1, 2, [[[3, 4]]]]))
console.log(file.flatten(['abc', 1, {1: 'hello', 2: 'world'}, [[[3, 4]]]]))
console.log(file.flatten('nestedArray', 'hello'))
