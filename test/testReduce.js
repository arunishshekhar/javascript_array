const reduceFile = require('../reduce')

const items = [1, 2, 3, 4, 5, 5];

result = reduceFile.reduce(items, (startingValue, CurrValue) => startingValue + CurrValue, 5);
console.log(result);

result = reduceFile.reduce(items, function (startingValue, CurrValue){return CurrValue%2 === 0 ? startingValue + 1 : startingValue; });
console.log(result);

result = reduceFile.reduce('items', (startingValue, CurrValue) => startingValue + CurrValue, 0);
console.log(result);

